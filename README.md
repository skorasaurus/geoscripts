geoscripts
==========

Assorted geo/map scripts that I use to make things easier for me and others. 

License: TBD; otherwise in file itself. 



untitled script: 

- removes GPS points near your house or other favorite places before uploading them to OpenStreetMap or wherever else, based on a radius of a certain size.
- uses gpsbabel
- TODO: write a function to randomize the radius size 